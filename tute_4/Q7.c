#include <stdio.h>
int findPrime(int n){

	int i,x=1;
	for(i=2; i<=n/2; ++i){
	
		if(n%i==0){
		x=0;
		}
	}
	return x;
}

int main(){

	int n1,n2,j,x;
	printf("Enter two numbers\n");
	scanf("%d %d",&n1,&n2);
	printf("Prime numbers between %d and %d are: \n",n1,n2);
	for(j=n1+1; j<n2; ++j){
	
		x=findPrime(j);
		if(x==1)
			printf("%d  \n",j);
	}	

return 0;
}
