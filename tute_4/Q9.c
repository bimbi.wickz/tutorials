#include <stdio.h>
int findArmstrong(int n);
void printArmstrong(int n1,int n2);

int findArmstrong(int n){

	int t,r,sum;
	t=n;
	sum=0;
	while(t!=0){
	
		r = t%10;
		sum = sum + r*r*r;
		t=t/10;
	}
	if(n==sum)
		return 1;
	else
		return 0;
}

void printArmstrong(int n1, int n2){

	while(n1<=n2){
	
		if(findArmstrong(n1)){
		
			printf("%d, ",n1);
		}
		n1++;
	}
}
int main(){

	int n1,n2;
	printf("Enter two numbers: \n");
	scanf("%d %d",&n1,&n2);
	printf("Armstrong numbers between %d and %d are: \n",n1,n2);
	printArmstrong(n1,n2);
return 0;
}
