#include <stdio.h>

#define pi 3.14

double findDiameter(int r){

	return 2*r;
}
double findCircumferance(int r){

	return 2*pi*r;
}
double findArea(int r){

	return pi*r*r;
}
int main(){
	int r;
	printf("Enter radius: \n");
	scanf("%d", &r);
	printf("Diameter = %.2f \n",findDiameter(r));
	printf("Circumferance = %.2f \n", findCircumferance(r));
	printf("Area = %.2f \n", findArea(r));
return 0;
}

