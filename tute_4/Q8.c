#include <stdio.h>
int findStrong(int n1, int n2);
int findFactor(int n);
int findStrong(int n1,int n2){

	int n, sum;
	while(n1!=n2){
	
		sum=0;
		n=n1;
		while(n!=0){
			sum = sum + findFactor(n%10);
			n = n/10;
			n1++;
		}
		if(n1==sum){
		
			printf("%d, ", n1);
		}
	}

}
int findFactor(int n){

	if(n==0)
		return 1;
else
		return (n*findFactor(n-1));
}
int main(){

	int n1,n2;
	printf("Enter the two numbers: \n");
	scanf("%d,%d", &n1,&n2);

	printf("Strong numbers between %d and %d are: \n",findStrong(n1,n2));
return 0;
}

