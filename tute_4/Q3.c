#include <stdio.h>
int findMax(int n1,int n2){

	return n1>n2 ? n1:n2;
}
int findMin(int n1,int n2){

	return n1>n2 ? n2:n1;
}
int main(){

	int n1,n2,min,max;
	printf("Enter two numbers: ");
	scanf("%d%d", &n1,&n2);

	printf("Maximum number= %d \n",findMax(n1,n2));
	printf("Minimum number= %d \n",findMin(n1,n2));

return 0;
}

